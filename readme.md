# Project for Course Computergraphics

#### Target is to Create a Engine to develop an Top Down Adventure

## Technologies
---
* Programming Language: C++, Used Commands
* OpenGL: Graphic
### Librarys:
* [GL**E**W](http://glew.sourceforge.net/): (v2.1.0) Using Opengl Extensions
* [GL**F**W ](https://www.glfw.org/): (v3.2.1) Window Creation, Input Events
* [GLM](https://glm.g-truc.net/0.9.9/index.html): (v0.9.9) Math Library, used for OpenGL.
* [RapidXML](http://rapidxml.sourceforge.net/): (v1.13) To Parse the "Tiled" Maps
* [FreeImage](http://freeimage.sourceforge.net/):(v3.18) Loading Images


## Architecture
* GameManager
---
* Component System
* Service Locator
---
* Assets
* Entity
* Graphics
* Scene
* Input
* Physics
---
* Tiled