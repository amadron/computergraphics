#version 420

uniform sampler2DArray tex;

in vec2 outuv;
in float currTileID;
void main()
{
	gl_FragColor = texture(tex,vec3(outuv, currTileID));
	//gl_FragColor = vec4(currTileID);
}