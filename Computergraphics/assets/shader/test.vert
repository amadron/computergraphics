#version 420

layout(location = 0) in vec3 position;
layout(location = 2) in vec2 uv;

out vec2 outuv;

void main()
{
	vec4 pos = vec4(position,1);
	gl_Position = pos;
	outuv = uv;
}