#version 420

uniform sampler2D tex;

in vec2 outuv;
void main()
{
	vec4 texColor = texture2D(tex, outuv);
	if(texColor.a < 0.1f)
	{
		discard;
	}
	gl_FragColor = texColor;
	//gl_FragColor = vec4(1);
}