#version 420

uniform sampler2D tex;

in vec2 outuv;
void main()
{
	gl_FragColor = vec4(texture2D(tex, outuv));
	//gl_FragColor = vec4(1,0,0,1);
}