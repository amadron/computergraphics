#version 420

uniform vec3 transformPosition;
uniform vec3 transformScale;
uniform mat4 cameraMatrix;

layout(location = 0) in vec3 position;
layout(location = 2) in vec2 uv;


out vec2 outuv;

void main()
{
	vec4 pos = cameraMatrix * vec4((position * transformScale) + transformPosition, 1);
	gl_Position = pos;
	outuv = uv;
}