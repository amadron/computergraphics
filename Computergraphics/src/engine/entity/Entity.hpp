#ifndef ENTITY_HPP
#define ENTITY_HPP
#include <string>
#include <vector>
#include <map>
#include <typeinfo>
#include "../components/IComponent.hpp"
#include "Transform.hpp"
using namespace std;
class IComponent;
class Entity
{
public:
	Transform transform;
	bool enabled = true;
	string name;
	Entity(string name)
	{
		this->name = name;
	}

	Entity(string name, Transform transform)
	{
		this->name = name;
		this->transform = transform;
	}

	void Update(double deltatime)
	{
		for (map<const type_info*, IComponent*>::iterator it = components.begin(); it != components.end(); it++)
		{
			if (it->second != nullptr && it->second->IsEnabled())
			{
				it->second->Update(deltatime);
			}
		}
	}

	template <class T>
	void AddComponent(T* component)
	{
		const type_info* type = &typeid(T);
		components[type] = (IComponent*)component;
		((IComponent*)component)->SetEntity(this);
	}

	void RemoveComponent(IComponent* component)
	{
		for (map<const type_info*, IComponent*>::iterator it = components.begin(); it != components.end(); it++)
		{
			if (it->second == component)
			{
				components.erase(it);
			}
		}
	}

	template<class T>
	T* GetComponentOfType()
	{
		const type_info* type = &typeid(T);
		return (T*) components[type];
	}

private:
	vector<IComponent*> componentsList;
	map<const type_info*, IComponent*> components;
};
#endif // !ENTITY_HPP
