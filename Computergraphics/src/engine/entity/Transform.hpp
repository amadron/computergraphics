#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP
#include "glm/glm.hpp"
using namespace glm;
class Transform
{
public:
	Transform()
	{
		position = vec3(0, 0, 0);
		scale = vec3(1, 1, 1);
		rotation = vec3(0, 0, 0);
	}
	vec3 position;
	vec3 scale;
	vec3 rotation;
};
#endif // !TRANSFORM_HPP
