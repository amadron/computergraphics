#include "AnimationClip.hpp"
#include "../components/renderable/CAnimationController.hpp"

void AnimationClip::Update(double deltatime)
{
	if (!isPlaying)
	{
		return;
	}
	for (int i = 0; i < tracks.size(); i++)
	{
		if (!tracks[i]->IsActive())
		{
			continue;
		}
		tracks[i]->Update(deltatime);
	}
	if (loop && IsFinished())
	{
		Start();
	}
}

void AnimationClip::Start()
{
	for (int i = 0; i < tracks.size(); i++)
	{
		isPlaying = true;
		tracks[i]->StartTrack();
	}
}

void AnimationClip::SetLoop(bool value)
{
	loop = value;
}

bool AnimationClip::IsLooped()
{
	return loop;
}

std::string AnimationClip::GetName()
{
	return name;
}

void AnimationClip::SetName(std::string name)
{
	this->name = name;
}

void AnimationClip::Continue()
{
	isPlaying = true;
}

void AnimationClip::Pause()
{
	isPlaying = false;
}

bool AnimationClip::IsFinished()
{
	bool finished = true;
	for (int i = 0; i < tracks.size(); i++)
	{
		if (!tracks[i]->IsFinished())
		{
			return false;
		}
	}
	return true;
}

void AnimationClip::SetAnimationController(CAnimationController* controller)
{
	this->controller = controller;
}

CAnimationController* AnimationClip::GetAnimationController()
{
	return controller;
}

void AnimationClip::SetAnimationTracks(std::vector<AnimationTrack*> tracks)
{
	this->tracks = tracks;
	for (int i = 0; i < tracks.size(); i++)
	{
		this->tracks[i]->SetClip(this);
		this->tracks[i]->StartTrack();
	}
}
void AnimationClip::AddAnimationTrack(AnimationTrack* track)
{
	track->SetClip(this);
	tracks.push_back(track);
}

const std::vector<AnimationTrack*> AnimationClip::GetTracks()
{
	return tracks;
}

bool AnimationClip::IsPlaying()
{
	return isPlaying;
}

void AnimationClip::ShowFrames()
{
	for (int i = 0; i < tracks.size(); i++)
	{
		tracks[i]->ShowFrame();
	}
}
