#ifndef ANIMATIONFRAME_HPP
#define ANIMATIONFRAME_HPP
#include <math.h>
class AnimationTrack;
class AnimationFrame
{
public:
	AnimationFrame(float durationTime);

	void Update(AnimationTrack* track, double deltatime);

	virtual void OnUpdate(AnimationTrack* track) {}

	void SetPassedTime(float time);

	float GetPassedTime();

	float GetFrameDuration();

	void StartFrame(AnimationTrack* track);

	virtual void OnStartFrame(AnimationTrack* track) {};
	/// <summary>
	/// Frame will apply it current Value/Action.
	/// For use, when AnimationClip will Changed and the The same Position shoud be applied
	/// </summary>
	virtual void ShowFrame(AnimationTrack* track) = 0;
	
protected:
	float passedTime = 0;
	float frameDuration = 0;
};
#endif // !ANIMATIONFRAME_HPP
