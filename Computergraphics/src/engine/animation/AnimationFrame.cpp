#include "AnimationFrame.hpp"
#include "AnimationTrack.hpp"
#include "../util/Debug.hpp"

AnimationFrame::AnimationFrame(float durationTime)
{
	this->frameDuration = durationTime;
}

void AnimationFrame::Update(AnimationTrack* track, double deltatime)
{
	passedTime += deltatime;
	bool finished = false;
	if (passedTime > frameDuration)
	{
		passedTime = frameDuration;
		finished = true;
	}
	OnUpdate(track);
	if (finished)
	{
		Debug::Log(this, __func__, __LINE__, Debug::INFO, "Frame Finished!");
		track->NextFrame();
	}

}

void AnimationFrame::SetPassedTime(float time)
{
	passedTime = time;
}

float AnimationFrame::GetPassedTime()
{
	return passedTime;
}

float AnimationFrame::GetFrameDuration()
{
	return frameDuration;
}

void AnimationFrame::StartFrame(AnimationTrack* track)
{
	passedTime = 0;
	OnStartFrame(track);
}