#ifndef ANIMATIONCLIP_HPP
#define ANIMATIONCLIP_HPP
#include "AnimationTrack.hpp"
#include <vector>
class CAnimationController;
class AnimationClip
{
public:
	void Update(double deltatime);

	void Start();

	std::string GetName();

	void SetName(std::string name);

	void Continue();

	void Pause();

	void SetAnimationController(CAnimationController* controller);
	
	CAnimationController* GetAnimationController();

	void SetAnimationTracks(std::vector<AnimationTrack*> tracks);
	void AddAnimationTrack(AnimationTrack* track);
	bool IsFinished();
	void SetLoop(bool value);
	bool IsLooped();
	const std::vector<AnimationTrack*> GetTracks();
	bool IsPlaying();
	void ShowFrames();
private:
	std::vector<AnimationTrack*> tracks;
	bool isPlaying = true;
	bool loop = false;
	std::string name;
	CAnimationController* controller;
};
#endif // !ANIMATIONCLIP_HPP
