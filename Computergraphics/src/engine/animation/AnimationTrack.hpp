#ifndef ANIMATIONTRACK_HPP
#define ANIMATIONTRACK_HPP
#include <vector>
#include "AnimationFrame.hpp"
//#include "AnimationClip.hpp"
class AnimationFrame;
class AnimationClip;
class AnimationTrack
{
public:
	void Update(double deltatime);

	void StartTrack();

	bool IsFinished();

	void SetActive(bool state);

	bool IsActive();

	int GetCurrentFrameNumber();

	int GetNumberOfFrames();

	void PreviousFrame();

	void NextFrame();

	void SetActiveFrame(int number);

	void SetClip(AnimationClip* clip);

	AnimationClip* GetClip();

	void SetFrames(std::vector<AnimationFrame*> frames);

	void AddFrame(AnimationFrame* frame);

	void ShowFrame();
private:
	std::vector<AnimationFrame*> frames;
	int currentFrameNumber = 0;
	bool isActive = true;
	AnimationClip* clip;
};
#endif // !ANIMATIONTRACK_HPP
