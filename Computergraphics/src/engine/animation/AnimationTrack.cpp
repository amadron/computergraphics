#include "AnimationTrack.hpp"
#include "AnimationClip.hpp"

void AnimationTrack::Update(double deltatime)
{
	if (frames.size() > currentFrameNumber)
	{
		frames[currentFrameNumber]->Update(this, deltatime);
	}
}

bool AnimationTrack::IsFinished()
{
	return(currentFrameNumber >= frames.size());
}

void AnimationTrack::StartTrack()
{
	currentFrameNumber = 0;
	if (frames.size() > 0)
	{
		frames[currentFrameNumber]->StartFrame(this);
	}
}

void AnimationTrack::SetActive(bool state)
{
	isActive = state;
}

bool AnimationTrack::IsActive()
{
	return isActive;
}

int AnimationTrack::GetCurrentFrameNumber()
{
	return currentFrameNumber;
}

int AnimationTrack::GetNumberOfFrames()
{
	return frames.size();
}

void AnimationTrack::PreviousFrame()
{
	currentFrameNumber--;
	if (currentFrameNumber < 0)
	{
		currentFrameNumber = 0;
	}
	frames[currentFrameNumber]->StartFrame(this);
}

void AnimationTrack::NextFrame()
{
	currentFrameNumber ++;
	if (frames.size() > currentFrameNumber)
	{
		frames[currentFrameNumber]->StartFrame(this);
	}
	
}

void AnimationTrack::SetActiveFrame(int number)
{
	if (frames.size() - 1 > number && number > 0)
	{
		currentFrameNumber = number;
	}

}

void AnimationTrack::SetClip(AnimationClip* clip)
{
	this->clip = clip;
}

AnimationClip* AnimationTrack::GetClip()
{
	return clip;
}

void AnimationTrack::SetFrames(std::vector<AnimationFrame*> frames)
{
	this->frames = frames;
}

void AnimationTrack::AddFrame(AnimationFrame* frame)
{
	frames.push_back(frame);
}

void AnimationTrack::ShowFrame()
{
	if (frames.size() > currentFrameNumber)
	{
		frames[currentFrameNumber]->ShowFrame(this);
	}
}