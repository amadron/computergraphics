#ifndef DEBUG_HPP
#define DEBUG_HPP
#include <string>
#include <gl/glew.h>
#include <gL/GL.h>
#include <gL/GLU.h>
#include <iostream>
using namespace std;
class Debug
{
public:
	enum LOGTYPE { ERROR, INFO, WARNING };

	template<typename T>
	static void CheckOpenGL(T classReference, const char functionName[], int line)
	{
		GLenum err = glGetError();
		if (err != GL_NO_ERROR)
		{
			//printf("OpenGL Error, Class: %s, Function: %pF, ErrorNo: %i: %s", typeid(classReference).name(), functionReference, err, glewGetErrorString(err));
			std::cout << "OpenGL Error, " << GetLogOrigin(classReference, functionName, line) << err << ": " << gluErrorString(err) << std::endl;
		}
	}

	template<typename T>
	static void Log(T classReference, const char functionName[], int line, LOGTYPE type ,string message)
	{
		std::cout << ERRORTYPENAMES[type] << " " << GetLogOrigin(classReference, functionName, line) << message << std::endl;
	}

	template<typename T>
	static string GetLogOrigin(T classReference, const char functionName[], int line)
	{
		return string(typeid(classReference).name()).append(", ").append(functionName).append("(), Line ").append(to_string(line)).append(": ");
	}
private:
	static const char* ERRORTYPENAMES[];
};
#endif // !DEBUG_HPP
