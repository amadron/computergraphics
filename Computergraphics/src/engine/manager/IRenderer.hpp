#ifndef IRENDERER_HPP
#define IRENDERER_HPP
#include "../entity/Entity.hpp"
#include "../graphics/camera/ICamera.hpp"
#include <vector>
class IRenderer
{
public:
	virtual void RenderEntity(Entity* entity) = 0;
	virtual void RenderEntitys(vector<Entity*> entitys) = 0;
	virtual void SetActiveCamera(ICamera* camera) = 0;
	virtual ICamera* GetActiveCamera() = 0;
};
#endif // !IRENDERER_HPP
