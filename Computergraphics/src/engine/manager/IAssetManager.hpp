#ifndef IASSETMANAGER
#define IASSETMANAGER
#include "../files/image.hpp"
#include "../graphics/mesh/VAO.hpp"
#include "../graphics/mesh/VBO.hpp"
#include "../graphics/mesh/AttributeLayout.hpp"
#include "../graphics/shader/IShaderProgram.hpp"
#include "../graphics/textures/ITexture.hpp"
#include "../geometry/Boundings2D.hpp"
#include "../entity/Entity.hpp"
#include "../util/Debug.hpp"
#include <string>
#include "../animation/AnimationClip.hpp"
using namespace std;
class IAssetManager
{
public:
	virtual ITexture* GetTexture2D(string path) = 0;
	virtual ITexture* GetTexture3D(string files[], size_t arraySize) = 0;
	virtual Image* LoadImage(string path) = 0;
	virtual string GetStringFromFile(string path) = 0;
	virtual VAO* GetQuad(float width, float height) = 0;
	virtual IShaderProgram* GetShader(string vertexShader, string fragmentShader) = 0;
	virtual void CleanUp() = 0;
	virtual Entity* Get2DEntity(string name, string texturePath) = 0;
	std::vector<glm::vec2> BoundingsToQuadUVArray(float left, float top, float right, float bottom)
	{
		std::vector<glm::vec2> uvs = {
		vec2(left,top),
		vec2(right,top),
		vec2(right,bottom),
		vec2(left,bottom) };
		return uvs;
	}

};
#endif // !IASSETMANAGER
