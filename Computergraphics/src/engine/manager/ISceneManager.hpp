#ifndef ISCENEMANAGER_HPP
#define ISCENEMANAGER_HPP
#include <string>
#include "../entity/Entity.hpp"
class ISceneManager
{
public:
	virtual void AddEntity(Entity* entity) = 0;
	virtual Entity* GetEntityByName(std::string name) = 0;
	virtual void RemoveEntity(Entity* entity) = 0;
	virtual const vector<Entity*> GetSceneList() = 0;

};
#endif // !ISCENEMANAGER_HPP
