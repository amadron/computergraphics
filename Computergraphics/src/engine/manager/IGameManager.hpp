#ifndef IGAMENAMAGER_HPP
#define IGAMEMANAGER_HPP
class IGameManager
{
	public:
		bool quit = false;
		virtual void Render(double deltatime) = 0;
		virtual void Update(double deltatime) = 0;
};
#endif // !IGAMENAMAGER_HPP
