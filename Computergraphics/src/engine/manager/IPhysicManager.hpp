#ifndef IPHYSICMANAGER_HPP
#define IPHYSICMANAGER_HPP
#include "../entity/Entity.hpp"
#include "../physics/Collision.hpp"
class IPhysicManager
{
public:
	virtual void UpdatePhysics(double deltatime) = 0;
	virtual bool CheckCollision(Entity* entity) = 0;
	virtual void CheckPosition(Entity* entity) = 0;
	virtual std::vector<Collision> GetOverlaps(Entity* entity) = 0;
};
#endif // !IPHYSICMANAGER_HPP
