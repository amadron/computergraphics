#ifndef IINPUTMANAGER_HPP
#define IINPUTMANAGER_HPP
#include "InputKeyCodes.hpp"
class IInputManager
{
public:
	enum KeyState { KEYSTATE_DOWN, KEYSTATE_PRESSED, KEYSTATE_RELEASED, KEYSTATE_UP, KEYSTATE_UNKNOWN };
	virtual KeyState GetKeyState(KeyCode key) = 0;
};
#endif // !IINPUTMANAGER_HPP
