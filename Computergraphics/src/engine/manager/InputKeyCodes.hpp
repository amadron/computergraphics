#ifndef INPUTKEYCODES_HPP
#define INPUTKEYCODES_HPP
//Key List from: https://docs.microsoft.com/de-de/windows/desktop/inputdev/virtual-key-codes
//Or http://kbdedit.com/manual/low_level_vk_list.html
enum KeyCode {
	Begin,
	//Mouse
	Mouse_Button_Left,
	Mouse_Button_Right,
	Mouse_Button_Middle,
	//Keyboard Special Keys
	Key_Backspace,
	Key_Tab,
	//Key_Clear,
	Key_Enter,
	//Key_Shift,
	//Key_Control,
	//Key_Alt,
	Key_Pause,
	Key_Caps_Log,
	Key_Escape,
	Key_Space,
	Key_Page_UP,
	Key_Page_Down,
	Key_End,
	Key_Home,
	Key_Arrow_Left,
	Key_Arrow_Right,
	Key_Arrow_Up,
	Key_Arrow_Down,
	//Key_Select,
	//Key_Print,
	Key_Print_Screen,
	Key_Insert,
	Key_Delete,
	//Key_Help,

	//Key_Windows_Left,
	//Key_Windows_Right,
	//Key_Application,

	Key_Numlock,
	Key_Scroll,

	Key_Shift_Left,
	Key_Shift_Right,
	Key_Control_Left,
	Key_Control_Right,
	Key_Alt_Left,
	Key_Alt_Right,

	//Numbers
	Key_0,
	Key_1,
	Key_2,
	Key_3,
	Key_4,
	Key_5,
	Key_6,
	Key_7,
	Key_8,
	Key_9,

	//Characters
	Key_A,
	Key_B,
	Key_C,
	Key_D,
	Key_E,
	Key_F,
	Key_G,
	Key_H,
	Key_I,
	Key_J,
	Key_K,
	Key_L,
	Key_M,
	Key_N,
	Key_O,
	Key_P,
	Key_Q,
	Key_R,
	Key_S,
	Key_T,
	Key_U,
	Key_V,
	Key_W,
	Key_X,
	Key_Y,
	Key_Z,

	//OEM Special Characters
	///<summary>
	///: ;
	///</summary>
	Key_OEM_1,
	///<summary>
	///> <
	///</summary>
	Key_OEM_102,
	///<summary>
	///? /
	///</summary>
	Key_OEM_2, 
	///<summary>
	///~ `
	///</summar>
	Key_OEM_3,
	///<summary>
	///{ [
	///</summary>
	Key_OEM_4,
	///<summary>
	///|\
	///</summary>
	Key_OEM_5,
	///<summary>
	///} ]
	///</summary>
	Key_OEM_6,
	///<summary>
	///" '
	///</summary>
	Key_OEM_7,
	///<summary>
	///� !
	///</summary>
	Key_OEM_8,
	
	///<summary>
	///< ,
	///</summary>
	Key_OEM_COMMA,
	///<summary>
	///_ -
	///</summary>
	Key_OEM_MINUS,
	///<summary>
	///> .
	///</summary>
	Key_OEM_PERIOD,
	///<summary>
	///+ =
	///</summary>
	Key_OEM_PLUS,

	//Numpad
	Key_KP_0,
	Key_KP_1,
	Key_KP_2,
	Key_KP_3,
	Key_KP_4,
	Key_KP_5,
	Key_KP_6,
	Key_KP_7,
	Key_KP_8,
	Key_KP_9,

	//Numpad Mathematics
	Key_KP_Multiply,
	Key_KP_Add,
	//Key_KP_Seperator,
	Key_KP_Substract,
	Key_KP_Decimal,
	Key_KP_Divide,
	Key_KP_Enter,

	//Function Keys
	Key_F1,
	Key_F2,
	Key_F3,
	Key_F4,
	Key_F5,
	Key_F6,
	Key_F7,
	Key_F8,
	Key_F9,
	Key_F10,
	Key_F11,
	Key_F12,

	End
};
#endif // !INPUTKEYCODES_HPP


