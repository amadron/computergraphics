#ifndef COLLISION_HPP
#define COLLISION_HPP
#include "glm/glm.hpp"
#include "../components/physics/CBoxCollider2D.hpp"
class Collision
{
public:
	Collision(glm::vec2 overlap, CBoxCollider2D* collider, CBoxCollider2D* otherCollider)
	{
		this->overlap = overlap;
		this->collider = collider;
		this->otherCollider = otherCollider;
	}

	glm::vec2 GetOverlap()
	{
		return overlap;
	}
	
	CBoxCollider2D* GetCollider()
	{
		return collider;
	}

	CBoxCollider2D* GetOtherCollider()
	{
		return otherCollider;
	}
private:
	glm::vec2 overlap;
	CBoxCollider2D* collider = nullptr;
	CBoxCollider2D* otherCollider = nullptr;
};
#endif // !COLLISION_HPP
