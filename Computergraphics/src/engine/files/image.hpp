#ifndef IMAGE_HPP
#define IMAGE_HPP
#include <stdint.h>
#include <gl/glew.h>
class Image
{

public:
	int format;
	int width;
	int height;
	void* data;
};
#endif // !IMAGE_HPP
