#ifndef LAYER_HPP
#define LAYER_HPP
#include <vector>
namespace Tiled
{
	class TiledLayer
	{
	public:
		int id;
		char* name;
		int width;
		int height;
		char* tileSet = NULL;
		int* layerTiles;
		std::vector<TiledLayer> layers;
	};
}
#endif