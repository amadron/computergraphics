#include "TiledManager.hpp"
#include "../rapidxml/rapidxml.hpp"
#include <string>
#include <iostream>
#include "glm/glm.hpp"
#include "../graphics/mesh/AttributeLayout.hpp"

using namespace std;
using namespace rapidxml;
using namespace Tiled;



//To do: Catching null Exceptions/Errors
TiledMap* TiledManager::GetMap(string filename)
{
	string mapFile = ServiceLocator::GetService<IAssetManager>()->GetStringFromFile(filename);
	char* mapChar = new char[mapFile.length() + 1];
	strcpy_s(mapChar, mapFile.length() + 1, mapFile.c_str());
	//cout << "XML: " << mapChar;
	xml_document<> doc;
	doc.parse<0>(&mapChar[0]);
	xml_node<>* node;
	node = doc.first_node("map");
	char* mapWidth = node->first_attribute("width")->value();
	char* mapHeight = node->first_attribute("height")->value();
	char* tileWidth = node->first_attribute("tilewidth")->value();
	char* tileHeight = node->first_attribute("tileheight")->value();

	TiledMap* map = new Tiled::TiledMap;
	map->width = atoi(mapWidth);
	map->height = atoi(mapHeight);
	map->tileheight = atoi(tileHeight);
	map->tilewidth = atoi(tileWidth);
	char* tileSet = nullptr;
	xml_node<>* tilesetNode = node->first_node("tileset");
	if (tilesetNode != nullptr)
	{
		tileSet = tilesetNode->first_attribute("source")->value();
	}
	vector<Tiled::TiledLayer> layerList;
	for (rapidxml::xml_node<> * layernode = node->first_node("layer"); layernode; layernode = layernode->next_sibling())
	{
		Tiled::TiledLayer tmpLayer = Tiled::TiledLayer();
		tmpLayer.name = layernode->first_attribute("name")->value();
		tmpLayer.tileSet = tileSet;
		int layerHeight = atoi(layernode->first_attribute("height")->value());
		int layerWidth = atoi(layernode->first_attribute("width")->value());
		tmpLayer.width = layerHeight;
		tmpLayer.height = layerWidth;
		rapidxml::xml_node<>* datanode = layernode->first_node("data");
		if (datanode != nullptr)
		{
			//cout << "Data: " << datanode->value() << "\n";
			const int arrayLength = layerHeight * layerWidth;
			int* tileData;
			char* layerData = datanode->value();
			char* token = nullptr;
			char* data = strtok_s(layerData, ",", &token);
			std::vector<char*> v;
			while (data)
			{
				//cout << "Data left: " << data << "\n";
				v.push_back(data);
				data = strtok_s(NULL, ",", &token);
			}
			tileData = new int[v.size()];
			for (int i = 0; i < v.size(); i++)
			{
				tileData[i] = atoi(v.at(i));
				//cout << "Added Tile Data: " << v.at(i) << " as Number: " << tileData[i] << "\n";
			}
			tmpLayer.layerTiles = tileData;
		}
		layerList.push_back(tmpLayer);
	}
	map->layerList = layerList;
	return map;
}

Tileset* TiledManager::GetTileset(std::string filename)
{
	string mapFile = ServiceLocator::GetService<IAssetManager>()->GetStringFromFile(filename);
	char* mapChar = new char[mapFile.length() + 1];
	strcpy_s(mapChar, mapFile.length() + 1, mapFile.c_str());
	//cout << "XML: " << mapChar;
	xml_document<> doc;
	doc.parse<0>(&mapChar[0]);
	xml_node<>* node;
	node = doc.first_node("tileset");
	if (node == nullptr)
	{
		return nullptr;
	}
	char* name = node->first_attribute("name")->value();
	char* tilewidth = node->first_attribute("tilewidth")->value();
	char* tileheight = node->first_attribute("tileheight")->value();
	char* tilecount = node->first_attribute("tilecount")->value();
	char* columns = node->first_attribute("columns")->value();
	
	Tileset* tileset = new Tiled::Tileset();
	tileset->name = name;
	tileset->tileWidth = atoi(tilewidth);
	tileset->tileHeight = atoi(tileheight);
	tileset->tileCount = atoi(tilecount);
	tileset->columns = atoi(columns);
	xml_node<>* image = node->first_node("image");
	if (image != nullptr)
	{
		char* sourceWidth = image->first_attribute("width")->value();
		char* sourceHeight = image->first_attribute("height")->value();
		char* sourcePath = image->first_attribute("source")->value();
		tileset->texturePath = sourcePath;
		tileset->textureHeight = atoi(sourceHeight);
		tileset->textureWidth = atoi(sourceWidth);
	}
	return tileset;
}

ITexture* TiledManager::GetLayerTexture(std::string path, int startValue, int endValue, std::string fileformat)
{
	int range = endValue - startValue;
	std::string* pathes = new std::string[range];
	for (int i = 0; i < range; i++)
	{
		int tmp = i + startValue;
		std::string tmpPath = path;
		tmpPath.append(std::to_string(tmp)).append(".").append(fileformat);
		*(pathes + i) = tmpPath;
	}
	return ServiceLocator::GetService<IAssetManager>()->GetTexture3D(pathes, range);
}

VAO* TiledManager::GetLayerVAO(TiledLayer layer)
{
	int width = layer.width;
	int height = layer.height;
	int amount = width * height;
	float* tilesID = new float[amount];

	float uvXFactor = 1.0f / width;
	float uvYFactor = 1.0f / height;

	glm::vec3* positions = new glm::vec3[amount];
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			int currIdx = i * width + j;
			*(positions + currIdx) = glm::vec3(1.0f * j, -1.0f * i, 0);
			*(tilesID + currIdx) = (float)layer.layerTiles[currIdx] - 1;
		}
	}
	VAO* vao = ServiceLocator::GetService<IAssetManager>()->GetQuad(1.0f, 1.0f);
	vao->instances = amount;
	vao->Activate();
	VBO* vbo = new VBO();
	vbo->attribID = AttributeLayout::PositionOffset;
	vbo->Activate();
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * width * height, positions, GL_STATIC_DRAW);
	glVertexAttribPointer(vbo->attribID, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(vbo->attribID);
	glVertexAttribDivisor(vbo->attribID, 1);
	vbo->Deactivate();
	vao->vboList.push_back(vbo);

	VBO* indexVBO = new VBO();
	indexVBO->attribID = AttributeLayout::terrainIndex;
	indexVBO->Activate();
	glVertexAttribPointer(indexVBO->attribID, 1, GL_FLOAT, GL_FALSE, 0, NULL);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * width * height, tilesID, GL_STATIC_DRAW);
	glEnableVertexAttribArray(indexVBO->attribID);

	glVertexAttribDivisor(indexVBO->attribID, 1);
	indexVBO->Deactivate();
	vao->vboList.push_back(indexVBO);

	vao->Deactivate();
	return vao;
}