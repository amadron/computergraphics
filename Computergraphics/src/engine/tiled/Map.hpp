#ifndef MAP_H
#define MAP_H
#include <vector>
#include "TiledLayer.hpp"
namespace Tiled
{
	class TiledMap
	{
	public:
		int width;
		int height;
		int tilewidth;
		int tileheight;
		std::vector<TiledLayer> layerList;
	};
}
#endif // !MAP_H
