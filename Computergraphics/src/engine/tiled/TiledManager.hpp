#ifndef TILEDMANAGER_HPP
#define TILEDMANAGER_HPP
#include "Map.hpp"
#include <string>
#include "../manager/IAssetManager.hpp"
#include "../graphics/textures/ITexture.hpp"
#include <vector>
#include "TiledLayer.hpp"
#include "Tileset.hpp"
#include "Tileset.hpp"

class TiledManager
{
public:
	Tiled::TiledMap* GetMap(std::string filename);
	Tiled::Tileset* GetTileset(std::string filename);
	ITexture* GetLayerTexture(std::string path, int startValue, int endValue, std::string fileformat);
	VAO* GetLayerVAO(Tiled::TiledLayer layer);
};
#endif