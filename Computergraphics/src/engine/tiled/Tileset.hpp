#ifndef TILESET_H
#define TILESET_H
namespace Tiled
{
	class Tileset
	{
	public:
		char* name;
		char* texturePath;
		int textureWidth;
		int textureHeight;
		int tileHeight;
		int tileWidth;
		int tileCount;
		int columns;
	};
}
#endif // !TILESET_H
