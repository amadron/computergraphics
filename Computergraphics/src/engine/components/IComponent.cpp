#include "IComponent.hpp"
#include "../entity/Entity.hpp"

bool IComponent::IsEnabled()
{
	return enabled;
}

Entity* IComponent::GetEntity()
{
	return owner;
}

void IComponent::SetEnabled(bool value)
{
	if (value != enabled)
	{
		if (value)
		{
			OnEnabled();
		}
		else
		{
			OnDisabled();
		}
	}
}

void IComponent::SetEntity(Entity* entity)
{
	if (owner != nullptr)
	{
		owner->RemoveComponent(this);
	}
	owner = entity;
}

void IComponent::OnEnabled()
{

}

void IComponent::Update(double deltatime)
{

}

void IComponent::OnDisabled()
{

}