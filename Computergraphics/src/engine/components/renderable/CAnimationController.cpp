#include "CAnimationController.hpp"
#include "../../animation/AnimationClip.hpp"

void CAnimationController::Update(double deltatime)
{
	if (activeClip != nullptr)
	{
		activeClip->Update(deltatime);
	}
}

void CAnimationController::SetAnimationClips(std::vector<AnimationClip*> clips)
{
	this->clips = clips;
	if (clips.size() > 0)
	{
		activeClip = clips[0];
	}
	for (int i = 0; i < clips.size(); i++)
	{
		clips[i]->SetAnimationController(this);
	}
}

void CAnimationController::AddAnimationClip(AnimationClip* clip)
{
	clips.push_back(clip);
	clip->SetAnimationController(this);
	if (clips.size() == 1)
	{
		activeClip = clip;
	}
}

AnimationClip* CAnimationController::GetAnimationClip(int idx)
{
	if (idx < clips.size())
	{
		return clips[idx];
	}
	else
	{
		return nullptr;
	}
}

AnimationClip* CAnimationController::GetAnimationClip(std::string name)
{
	for (int i = 0; i < clips.size(); i++)
	{
		if (clips[i]->GetName() == name)
		{
			return clips[i];
		}
	}
	return nullptr;
}

void CAnimationController::SetActiveClip(std::string name)
{
	for (int i = 0; i < clips.size(); i++)
	{
		if (clips[i]->GetName() == name)
		{
			activeClip = clips[i];
			activeClip->ShowFrames();
		}
	}
}

AnimationClip* CAnimationController::GetActiveAnimationClip()
{
	return activeClip;
}

void CAnimationController::StartActiveClip()
{
	if (activeClip)
	{
		activeClip->Start();
	}
}

void CAnimationController::PauseActiveClip()
{
	if (activeClip)
	{
		activeClip->Pause();
	}
}

void CAnimationController::ContinueActiveClip()
{
	if (activeClip)
	{
		activeClip->Continue();
	}
}

void CAnimationController::RemoveAnimationClip(AnimationClip* clip)
{
	for (int i = 0; i < clips.size(); i++)
	{
		if (clips[i] == clip)
		{
			clips.erase(clips.begin() + i);
		}
	}
}