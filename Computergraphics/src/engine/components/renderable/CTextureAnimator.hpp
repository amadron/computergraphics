#ifndef CTEXTUREANIMATOR_HPP
#define CTEXTUREANIMATOR_HPP
#include "../IComponent.hpp"
#include "../../graphics/textures/animation/UVGrid.hpp"
#include "../../components/renderable/CRenderable.hpp"
#include "../../manager/IAssetManager.hpp"
#include "../../graphics/mesh/AttributeLayout.hpp"
class CTextureAnimator : public IComponent
{
public:
	CTextureAnimator(UVGrid* grid)
	{
		this->grid = grid;
	}

	CTextureAnimator(int gridElementsX, int gridElementsY)
	{
		this->grid = new UVGrid(gridElementsX, gridElementsY);
	}

	UVGrid* GetGrid()
	{
		return grid;
	}

	void SetGrid(UVGrid* grid)
	{
		this->grid = grid;
	}

	void SetUVsOfElement(int x, int y)
	{
		Boundings2D bounds = grid->GetUVBounds(x, y);
		SetUVs(bounds);
	}

	void SetUVsOfElement(int number)
	{
		Boundings2D bounds = grid->GetUVBounds(number);
		SetUVs(bounds);
	}

private:
	UVGrid* grid;
	void SetUVs(Boundings2D bounds)
	{
		CRenderable* renderable = GetEntity()->GetComponentOfType<CRenderable>();
		if (!renderable || !renderable->mesh)
		{
			return;
		}
		std::vector<glm::vec2> uvs = ServiceLocator::GetService<IAssetManager>()->BoundingsToQuadUVArray(bounds.left, bounds.top, bounds.right, bounds.bottom);
		VBO* vbo = renderable->mesh->GetBufferByAttributeIndex(AttributeLayout::UV);
		glm::vec2 uvs2[] = {
			glm::vec2(bounds.left,bounds.top),
			glm::vec2(bounds.right,bounds.top),
			glm::vec2(bounds.right,bounds.bottom),
			glm::vec2(bounds.left,bounds.bottom)
		};

		vbo->SetAttribute(&uvs[0], uvs.size() * sizeof(glm::vec2));
	}
};
#endif // !CTEXTUREANIMATOR_HPP
