#ifndef CANIMATIONCONTROLLER_HPP
#define CANIMATIONCONTROLLER_HPP
#include "../IComponent.hpp"
#include <vector>
class AnimationClip;
class CAnimationController : public IComponent
{
public:
	virtual void Update(double deltatime);

	void SetAnimationClips(std::vector<AnimationClip*> clips);
	AnimationClip* GetAnimationClip(int index);
	AnimationClip* GetAnimationClip(std::string name);
	void SetActiveClip(std::string name);
	AnimationClip* GetActiveAnimationClip();
	void StartActiveClip();
	void PauseActiveClip();
	void ContinueActiveClip();
	void AddAnimationClip(AnimationClip* clip);
	void RemoveAnimationClip(AnimationClip* clip);
private:
	std::vector<AnimationClip*> clips;
	AnimationClip* activeClip = nullptr;
};
#endif // !CANIMATOR_HPP
