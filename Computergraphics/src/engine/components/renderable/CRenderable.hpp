#ifndef CRENDERABLE_HPP
#define CRENDERABLE_HPP
#include "../IComponent.hpp"
#include "../../graphics/mesh/VAO.hpp"
#include "../../graphics/textures/ITexture.hpp"
#include "../../graphics/shader/IShaderProgram.hpp"
class CRenderable : public IComponent
{
public:
	CRenderable(IShaderProgram* shader, ITexture* texture, VAO* mesh)
	{
		this->shader = shader;
		this->texture = texture;
		this->mesh = mesh;
	}
	IShaderProgram* shader;
	ITexture* texture;
	VAO* mesh;
};
#endif // !CRENDERABLE_HPP

