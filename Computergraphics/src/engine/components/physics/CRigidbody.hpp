#ifndef CRIGIDBODY_HPP
#define CRIGIDBODY_HPP
#include "../IComponent.hpp"
#include "glm\glm.hpp"
class CRigidBody : public IComponent
{
public:
	glm::vec3 acceleration = glm::vec3(0,0,0);
	bool constraintXAxis = false;
	bool constraintYAxis = false;
	bool constraintZAxis = false;
};
#endif // !CRIGIDBODY_HPP
