#ifndef CBOXCOLLIDER2D_HPP
#define CBOXCOLLIDER_HPP
#include "../IComponent.hpp"
#include "../../geometry/Box2D.hpp"
class CBoxCollider2D : public IComponent
{
public:
	bool trigger = false;
	CBoxCollider2D(Box2D* box)
	{
		this->box = box;
	}
	
	Box2D* GetBox()
	{
		return box;
	}

private:
	Box2D* box = nullptr;

};
#endif // !CBOXCOLLIDER2D_HPP
