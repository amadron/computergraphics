#ifndef ICOMPONENT_HPP
#define ICOMPONENT_HPP
#include "../serviceslocator/ServiceLocater.hpp"
class Entity;
class IComponent
{
public:
	bool IsEnabled();
	Entity* GetEntity();
	void SetEnabled(bool value);
	void SetEntity(Entity* entity);

	virtual void OnEnabled();
	virtual void Update(double deltatime);
	virtual void OnDisabled();
private:
	Entity* owner;
	bool enabled = true;
};
#endif