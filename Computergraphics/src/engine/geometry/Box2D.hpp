#ifndef BOX2D_HPP
#define BOX2D_HPP
#include "../entity/Transform.hpp"
#include "glm\glm.hpp"
#include <math.h>
class Box2D
{
public:
	Box2D(Transform* transform, float width, float height)
	{
		this->transform = transform;
		SetSize(width, height);
	}

	void SetOffset(glm::vec3 position)
	{

	}

	glm::vec3 GetOffset()
	{
		return offset;
	}

	void SetSize(float width, float height)
	{
		this->width = width;
		this->height = height;
		CalculateBounds();
	}

	float Width()
	{
		return width;
	}

	float Height()
	{
		return height;
	}

	float Left()
	{
		return left + transform->position.x + offset.x;
	}

	float Right()
	{
		return right + transform->position.x + offset.x;
	}

	float Top()
	{
		return top + transform->position.y + offset.y;
	}

	float Bottom()
	{
		return bottom + transform->position.y + offset.y;
	}

	//https://stackoverflow.com/questions/306316/determine-if-two-rectangles-overlap-each-other
	bool DoesOverlap(Box2D* other)
	{
		return !(Left() >= other->Right()
			|| Right() <= other->Left()
			|| Top() <= other->Bottom()
			|| Bottom() >= other->Top());

	}
	/*
	Overlap: https://stackoverflow.com/questions/9324339/how-much-do-two-rectangles-overlap
	*/

	glm::vec2 GetOverlap(Box2D* other)
	{
		
		float x = min(Right(), other->Right()) - max(Left(), other->Left());
		float y = min(Top(), other->Top()) - max(Bottom(), other->Bottom());
		return glm::vec2(x, y);
	}

	glm::vec2 GetOverlapWithCoefficient(Box2D* other)
	{
		float x = min(Right(), other->Right()) - max(Left(), other->Left());
		float y = min(Top(), other->Top()) - max(Bottom(), other->Bottom());
		glm::vec2 result;
		//SetCoefficient of overlapp
		result.x = transform->position.x > other->transform->position.x ? x : -x;
		result.y = transform->position.y > other->transform->position.y ? y : -y;
		return result;
	}

	void UndoOverlap(Box2D* other)
	{
		glm::vec2 overlap = GetOverlapWithCoefficient(other);
		if (abs(overlap.x) < abs(overlap.y))
		{
			transform->position.x += overlap.x;
		}
		else
		{
			transform->position.y += overlap.y;
		}
	}
private:
	void CalculateBounds()
	{
		right = width / 2;
		left = -right;
		top = height / 2;
		bottom = -top;
	}
	Transform* transform;
	glm::vec3 offset = glm::vec3();
	float width;
	float height;
	float left;
	float right;
	float top;
	float bottom;
};
#endif // !BOX2D_HPP
