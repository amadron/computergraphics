#ifndef BOUNDINGS2D_HPP
#define BOUNDINGS2D_HPP
class Boundings2D
{
public:
	Boundings2D() {}

	Boundings2D(float left, float top, float right, float bottom)
	{
		this->left = left;
		this->right = right;
		this->top = top;
		this->bottom = bottom;
	}

	float left = 0;
	float right = 0;
	float top = 0;
	float bottom = 0;
};
#endif // !BOUNDINGS_HPP
