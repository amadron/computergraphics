#ifndef SERVICELOCATOR_HPP
#define SERVICELOCATOR_HPP
#include <typeinfo>
#include <map>
#include "../manager/IInputManager.hpp"
class ServiceLocator
{

public:
	ServiceLocator()
	{
		
	}
	//template <class T>
	//static T& GetService();
	template <class Base, class T>
	static void Provide(T* service)
	{
		const std::type_info* info = &typeid(Base);
		serviceList[info] = service;
	}

	template <class T>
	static T* GetService()
	{
		const std::type_info* info = &typeid(T);
		return (T*)serviceList[info];
	}

private:
	static std::map<const std::type_info*, void*> serviceList;

};
#endif // !SERVICELOCATOR_HPP
