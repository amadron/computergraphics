#include "VBO.hpp"

VBO::VBO()
{	
	GLuint glid;
	glGenBuffers(1, &glid);
	id = glid;
}

void VBO::Activate()
{
	glBindBuffer(GL_ARRAY_BUFFER, id);
}


void VBO::Deactivate()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VBO::SetAttribute(void* values, size_t size)
{
	Activate();
	glBufferData(GL_ARRAY_BUFFER, size, values, GL_STATIC_DRAW);
	Deactivate();
}