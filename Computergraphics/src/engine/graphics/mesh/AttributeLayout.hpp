#ifndef ATTRIBUTELAYOUT_HPP
#define ATTRIBUTELAYOUT_HPP
/* This Class defines the Default Attribut layout Numbers
*	which are used in My Shaders.
*/


enum AttributeLayout
{
	Position, Normal, UV, PositionOffset, terrainIndex
};

#endif // !VBOIDS_HPP
