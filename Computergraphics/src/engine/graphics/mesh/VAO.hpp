#ifndef VAO_H
#define VAO_H
#include <vector>
#include "VBO.hpp"
class VAO
{
public:
	int id = 0;
	int instances = 0;
	std::vector<VBO*> vboList;
	VAO(int id);
	VBO* GetBufferByAttributeIndex(int attributeIDx);
	void Activate();
	void Deactivate();
	void DeleteVAO();
};

#endif // !VAO
