#ifndef VBO_HPP
#define VBO_HPP

#include "GL/glew.h"

class VBO
{
public:
	VBO();
	int id;
	int attribID;
	void Activate();
	void Deactivate();
	void SetAttribute(void* values, size_t size);
};

#endif // !VBO_HPP
