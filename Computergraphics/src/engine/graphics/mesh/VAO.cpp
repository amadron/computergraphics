#include "GL/glew.h"
#include <list>
#include <vector>
#include "VAO.hpp"
#include "GL/glew.h"
	
	VAO::VAO(int id)
	{
		this->id = id;
		
	}

	VBO* VAO::GetBufferByAttributeIndex(int attributeIDx)
	{
		VBO* result = nullptr;
		for (int i = 0; i < vboList.size(); i++)
		{
			if (vboList[i]->attribID == attributeIDx)
			{
				return vboList[i];
			}
		}
		return result;
	}

	void VAO::Activate()
	{
		glBindVertexArray(id);
		for (int i = 0; i < vboList.size(); i++)
		{
			glEnableVertexAttribArray(vboList[i]->attribID);
		}

	}

	void VAO::Deactivate()
	{
		glBindVertexArray(0);
	}

	void VAO::DeleteVAO()
	{
		glDeleteVertexArrays(1, (GLuint *) &id);
	}

