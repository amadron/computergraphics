#include "GLEWTexture2D.hpp"


GLEWTexture2D::GLEWTexture2D(int id, GLenum textureTypeTarget)
{
	this->id = id;
	this->textureType = textureTypeTarget;
}
// Geerbt �ber ITexture2D
void GLEWTexture2D::Activate()
{
	glBindTexture(textureType, id);
}

void GLEWTexture2D::Deactivate()
{
	glBindTexture(textureType, 0);
}
