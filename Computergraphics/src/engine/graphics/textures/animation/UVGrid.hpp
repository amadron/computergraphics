#ifndef UVGRID_HPP
#define UVGRID_HPP
#include "../../../geometry/Boundings2D.hpp"
class UVGrid
{
private:
	int amountX;
	int amountY;
	float boundLeft = 0;
	float boundTop = 1;
	float boundBottom = 0;
	float boundRight = 1;
	int totalAmount = 1;
	float cellWidth;
	float cellHeight;
	void RecalculateValues();

public:
	UVGrid();
	UVGrid(int amountX, int amountY);
	void SetAmountOfCells(int x, int y);
	void SetAmountOfXCells(int x);
	void SetAmountOfYCells(int y);
	void SetUVBoundings(float left, float bottom, float top, float right);
	float GetCellWidth();
	float GetCellHeight();
	int GetTotalAmountOfCells();
	int GetNextIndex(int idx);
	int GetPreviousIndex(int idx);
	Boundings2D GetUVBounds(int idx);
	Boundings2D GetUVBounds(int x, int y);
};
#endif // !UVGRID_HPP
