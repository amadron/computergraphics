#include "UVGrid.hpp"

UVGrid::UVGrid()
{
	SetAmountOfCells(1, 1);
}

UVGrid::UVGrid(int amountX, int amountY)
{
	SetAmountOfCells(amountX, amountY);
}

void UVGrid::SetAmountOfCells(int x, int y)
{
	this->amountX = x;
	this->amountY = y;
	RecalculateValues();
}

void UVGrid::SetAmountOfXCells(int x)
{
	this->amountX = x;
	RecalculateValues();
}

void UVGrid::SetAmountOfYCells(int y)
{
	this->amountY = y;
	RecalculateValues();
}

float UVGrid::GetCellWidth()
{
	return this->cellWidth;
}

float UVGrid::GetCellHeight()
{
	return this->cellHeight;
}

void UVGrid::SetUVBoundings(float left, float bottom, float top, float right)
{
	boundLeft = left;
	boundRight = right;
	boundTop = top;
	boundBottom = bottom;
	RecalculateValues();
}

int UVGrid::GetTotalAmountOfCells()
{
	return this->totalAmount;
}

int UVGrid::GetNextIndex(int idx)
{
	return (idx + 1) % totalAmount ;
}

int UVGrid::GetPreviousIndex(int idx)
{
	return (idx - 1) % totalAmount;
}

Boundings2D UVGrid::GetUVBounds(int idx)
{
	int row = idx / amountX;
	float bottom = boundBottom + row * cellHeight;
	float top = bottom + cellHeight;
	int elementX = idx % amountX;
	float left = boundLeft + elementX * cellWidth;
	float right = left + cellWidth;

	return Boundings2D(left, top, right, bottom);
}

void UVGrid::RecalculateValues()
{
	totalAmount = amountX * amountY;
	float spanX = boundRight - boundLeft;
	float spanY = boundTop - boundBottom;
	cellWidth = spanX / amountX;
	cellHeight = spanY / amountY;
}

Boundings2D UVGrid::GetUVBounds(int x, int y)
{
	float bottom = boundBottom + y * cellHeight;
	float top = bottom + cellHeight;
	float left = boundLeft + x * cellWidth;
	float right = left + cellWidth;

	return Boundings2D(left, top, right, bottom);
}

