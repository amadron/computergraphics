#ifndef ITEXTURE
#define	ITEXTURE
class ITexture
{
public:
	int id;
	virtual void Activate() = 0;
	virtual void Deactivate() = 0;
};
#endif