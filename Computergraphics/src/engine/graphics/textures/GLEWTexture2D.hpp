#ifndef GLEWTEXTURE2D_HPP
#define	GLEWTEXTURE2D_HPP
#include "ITexture.hpp"
#include <GL\glew.h>

class GLEWTexture2D : ITexture 
{

public:
	GLEWTexture2D(int id, GLenum target);
	void Activate() override;
	void Deactivate() override;
private:
	GLenum textureType;
};
#endif