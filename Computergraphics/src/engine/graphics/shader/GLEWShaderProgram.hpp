#ifndef GLEWSHADERPROGRAM
#define GLEWHADERPROGRAM
#include "IShaderProgram.hpp"

class GLEWShaderProgram : IShaderProgram
{

public:
	// Geerbt �ber IShaderProgramm
	GLEWShaderProgram(int id);
	void Activate() override;
	void Deactivate() override;

};
#endif