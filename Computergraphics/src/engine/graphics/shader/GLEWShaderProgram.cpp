#include "GLEWShaderProgram.hpp"
#include "GL/glew.h"

GLEWShaderProgram::GLEWShaderProgram(int id)
{
	this->id = id;
}

// Geerbt �ber IShaderProgramm
void GLEWShaderProgram::Activate()
{
	glUseProgram(id);
}

void GLEWShaderProgram::Deactivate()
{
	glUseProgram(0);
}
