#ifndef SHADERPROGRAM_HPP
#define SHADERPROGRAM_HPP
class IShaderProgram
{

public:
	int id;
	virtual void Activate() = 0;
	virtual void Deactivate() = 0;
};

#endif // !SHADERPROGRAM_HPP
