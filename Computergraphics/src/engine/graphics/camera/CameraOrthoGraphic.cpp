#include "CameraOrthoGraphic.hpp"
#include "glm/gtc/matrix_transform.hpp"

CameraOrthoGraphic::CameraOrthoGraphic(glm::vec3 position, glm::vec3 rotation, float width, float height, float zPlaneNear, float zPlaneFar)
{
	this->position = position;
	this->rotation = rotation;
	this->width = width;
	this->height = height;
	this->zPlaneNear = zPlaneNear;
	this->zPlaneFar = zPlaneFar;
	this->forward = glm::vec3(0, 0, -1.0f);
	this->up = glm::vec3(0, 1.0f, 0);
}

glm::mat4x4 CameraOrthoGraphic::GetCameraMatrix()
{
	return GetProjectionMatrix() * GetViewMatrix();
}

glm::mat4x4 CameraOrthoGraphic::GetProjectionMatrix()
{
	float halfX = width / 2;
	float halfY = height / 2;
	return glm::ortho(-halfX, halfX, -halfY, halfY, zPlaneNear, zPlaneFar);
}

glm::mat4x4 CameraOrthoGraphic::GetViewMatrix()
{
	glm::mat4 mat = glm::mat4();
	glm::vec3 radians = glm::radians(this->rotation);
	for (int i = 0; i < 4; i++)
	{
		mat[i][i] = 1;
	}
	glm::mat4 rotationMat(1);

	rotationMat = glm::rotate(rotationMat, (glm::mediump_float32) radians.y, glm::vec3(0, 1, 0));
	rotationMat = glm::rotate(rotationMat, (glm::mediump_float32) radians.x, glm::vec3(1, 0, 0));
	rotationMat = glm::rotate(rotationMat, (glm::mediump_float32) radians.z, glm::vec3(0, 0, 1));
	glm::vec4 tmpForward(forward, 1);
	tmpForward = rotationMat * tmpForward;
	//tmpForward = glm::normalize(tmpForward);
	glm::vec4 tmpUp(up, 1);
	tmpUp = rotationMat * tmpUp;
	//tmpUp = glm::normalize(tmpUp);
	glm::vec3 center = this->position + glm::vec3(tmpForward.x, tmpForward.y, tmpForward.z);
	glm::mat4 lookat = glm::lookAt(this->position, center, glm::vec3(tmpUp.x, tmpUp.y, tmpUp.z));
	return lookat;// *translateMatrix;
}