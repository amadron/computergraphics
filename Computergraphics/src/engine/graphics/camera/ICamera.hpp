#ifndef ICAMERA_HPP
#define ICAMERA_HPP
#include "glm/glm.hpp"
class ICamera
{
public:
	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 forward;
	glm::vec3 up;
	virtual glm::mat4x4 GetCameraMatrix() = 0;
	virtual glm::mat4x4 GetProjectionMatrix() = 0;
	virtual glm::mat4x4 GetViewMatrix() = 0;
};
#endif // !ICAMERA_HPP
