#ifndef CAMERAORTHOGRAPHIC_HPP
#define CAMERAORTHOGRAPHIC_HPP
#include "ICamera.hpp"
class CameraOrthoGraphic : ICamera
{
private:
	float width;
	float height;
	float zPlaneNear;
	float zPlaneFar;

public:
	CameraOrthoGraphic(glm::vec3 position, glm::vec3 rotation, float width, float height, float zPlaneNear, float zPlaneFar);
	virtual glm::mat4x4 GetCameraMatrix() override;
	virtual glm::mat4x4 GetProjectionMatrix() override;
	virtual glm::mat4x4 GetViewMatrix() override;
};
#endif // !CAMERAORTHOGRAPHIC_HPP
