#ifndef CPLAYERINPUT_HPP
#define CPLAYERINPUT_HPP
#include "../../engine/components/IComponent.hpp"
#include "../../engine/manager/IInputManager.hpp"
#include "../../engine/manager/IPhysicManager.hpp"
#include "../components/CCharacterController.hpp"
#include "glm/glm.hpp"
#include "../../engine/entity/Entity.hpp"

class CPlayerInput : public IComponent
{
	virtual void Update(double deltatime)
	{
		IInputManager* input = ServiceLocator::GetService<IInputManager>();
		CCharacterController* charControl = GetEntity()->GetComponentOfType<CCharacterController>();
		if (!charControl)
		{
			return;
		}
		glm::vec3 movement = glm::vec3(0, 0, 0);
		float moveSpeed = (float)deltatime * 1;
		bool move = false;
		if (input->GetKeyState(KeyCode::Key_W) == input->KEYSTATE_DOWN)
		{
			movement.y += moveSpeed;
			move = true;
		}
		if (input->GetKeyState(KeyCode::Key_S) == input->KEYSTATE_DOWN)
		{
			movement.y -= moveSpeed;
			move = true;
		}
		if (input->GetKeyState(KeyCode::Key_A) == input->KEYSTATE_DOWN)
		{
			movement.x -= moveSpeed;
			move = true;
		}
		if (input->GetKeyState(KeyCode::Key_D) == input->KEYSTATE_DOWN)
		{
			movement.x += moveSpeed;
			move = true;
		}
		if (!move)
		{
			charControl->Idle();
		}
		else
		{
			charControl->Move(movement);
		}
		UpdateCamera();
	}

	void UpdateCamera()
	{
		ICamera* cam = ServiceLocator::GetService<IRenderer>()->GetActiveCamera();
		if (!cam)
		{
			return;
		}
		glm::vec4 pPos = glm::vec4(GetEntity()->transform.position, 1);
		glm::vec4 tPos = cam->GetCameraMatrix() * pPos;
		glm::vec3 camMove = glm::vec3();
		float camMoveBound = 0.75f;
		if (tPos.x < -camMoveBound)
		{
			camMove.x = tPos.x + camMoveBound;
		}
		else if (tPos.x > camMoveBound)
		{
			camMove.x = tPos.x - camMoveBound;
		}
		if (tPos.y < -camMoveBound)
		{
			camMove.y = tPos.y + camMoveBound;
		}
		else if (tPos.y > camMoveBound)
		{
			camMove.y = tPos.y - camMoveBound;
		}
		cam->position += camMove;
	}
};
#endif // !CPLAYERINPUT_HPP

