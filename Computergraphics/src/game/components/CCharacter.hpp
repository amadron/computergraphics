#ifndef CCHARACTER_HPP
#define CCHARACTER_HPP
#include "../../engine/components/IComponent.hpp"
#include <glm\glm.hpp>
class CCharacter : IComponent
{
public:
	CCharacter(std::string name, int health)
	{
		this->health = health;
		this->name = name;
	}

	bool IsAttackable()
	{
		return attackable;
	}

	void SetAttackable(bool value)
	{
		attackable = value;
	}

	float GetSpeed()
	{
		return speed;
	}

	void SetSpeed(float speed)
	{
		this->speed = speed;
	}

	void SetDirection(glm::vec2 dir)
	{
		this->direction = dir;
	}

	glm::vec2 GetDirection()
	{
		return direction;
	}

private:
	glm::vec2 direction = glm::vec2();
	bool attackable = true;
	std::string name;
	int maxHealth = 100;
	int health = 0;
	float speed = 2;
	//Stats
	int attack;
	int defence;

};
#endif // !CCHARACTER_HPP

