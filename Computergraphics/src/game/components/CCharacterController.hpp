#ifndef CCHARACTERCONTROLLER_HPP
#define CCHARACTERCONTROLLER_HPP
#include "../../engine/components/IComponent.hpp"
#include "../../engine/components/renderable/CAnimationController.hpp"
#include "../../engine/serviceslocator/ServiceLocater.hpp"
#include <glm\glm.hpp>
#include "../../engine/entity/Entity.hpp"
#include "../../engine/manager/IPhysicManager.hpp"
class CCharacterController : public IComponent
{
public:
	virtual void Update(double deltatime)
	{

	}

	void Attack()
	{

	}

	void AttackEnd()
	{

	}

	void Evade()
	{

	}

	void EvadeEnd()
	{

	}

	void Move(glm::vec3 dir)
	{
		if (dir.y > 0)
		{
			PlayAnimation("up");
		}
		if (dir.y < 0)
		{
			PlayAnimation("down");
		}
		if (dir.x > 0)
		{
			PlayAnimation("right");
		}
		if (dir.x < 0)
		{
			PlayAnimation("left");
		}
		Entity* entity = GetEntity();
		entity->transform.position += dir;
		entity->transform.position.z = -entity->transform.position.y;
		ServiceLocator::GetService<IPhysicManager>()->CheckPosition(entity);
	}

	void Idle()
	{
		CAnimationController* anim = GetEntity()->GetComponentOfType<CAnimationController>();
		if (anim)
		{
			anim->PauseActiveClip();
		}
	}
	
	void PlayAnimation(std::string name)
	{
		CAnimationController* anim = GetEntity()->GetComponentOfType<CAnimationController>();
		if (anim)
		{
			anim->SetActiveClip(name);
			anim->ContinueActiveClip();
		}
	}
private:
	bool attack = false;
};
#endif // !CCHARACTERCONTROLLER_HPP
