#ifndef UVGRIDFRAME_HPP
#define UVGRIDFRAME_HPP
#include "../../engine/animation/AnimationFrame.hpp"
#include "../../engine/entity/Entity.hpp"
#include "../../engine/components/renderable/CTextureAnimator.hpp"
#include "../../engine/components/renderable/CRenderable.hpp"
class UVGridFrame : public AnimationFrame
{
public:
	UVGridFrame(int elementNumberX, int elementNumberY, float durationTime) : AnimationFrame(durationTime)
	{
		this->elementNumberX = elementNumberX;
		this->elementNumberY = elementNumberY;
	}

	virtual void OnStartFrame(AnimationTrack* track)
	{
		ShowFrame(track);

	}

	virtual void ShowFrame(AnimationTrack* track)
	{
		AnimationClip* clip = track->GetClip();
		CAnimationController* controller = clip->GetAnimationController();

		Entity* entity = controller->GetEntity();
		if (entity == nullptr)
		{
			return;
		}
		CTextureAnimator* textureAnimator = entity->GetComponentOfType<CTextureAnimator>();
		if (textureAnimator == nullptr)
		{
			return;
		}
		textureAnimator->SetUVsOfElement(elementNumberX, elementNumberY);
	}

private:
	int elementNumberX = 0;
	int elementNumberY = 0;
};
#endif // !UVGRIDFRAME_HPP
