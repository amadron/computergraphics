#ifndef ASSETMANAGER_HPP
#define ASSETMANAGER_HPP
#include "../../engine/manager/IAssetManager.hpp"
#include <FreeImage.h>
class AssetManager : IAssetManager
{
private:
	string appPath;
public:
	AssetManager(string appPath);
	string GetStringFromFile(string path) override;
	ITexture* GetTexture2D(string path) override;
	ITexture* GetTexture3D(string files[], size_t arraySize) override;
	Image* LoadImage(string path) override;
	VAO* GetQuad(float width, float height) override;
	IShaderProgram* GetShader(string vertexShader, string fragmentShader) override;
	string GetAppPath();
	void CleanUp() override;
	Entity* Get2DEntity(string name, string texturePath);
};
#endif // !ASSETMANAGER_HPP
