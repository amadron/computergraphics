#include "InputManagerGLFW.hpp"
InputManagerGLFW::InputManagerGLFW(GLFWwindow* window)
{
	this->window = window;
	std::map<KeyCode, int>::iterator it;
	for (it = keyToGLFWMap.begin(); it != keyToGLFWMap.end(); it++)
	{
		keyStates[it->first] = KeyState::KEYSTATE_UP;
		glfwToKeyMap[it->second] = it->first;
	}

}

void InputManagerGLFW::KeyCallBack(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	std::map<int, KeyCode>::iterator it = glfwToKeyMap.find(key);
	if (it != glfwToKeyMap.end())
	{
		KeyState curr = keyStates[it->second];
		changedKeys.push_back(it->second);
		if (action == GLFW_PRESS && curr == KeyState::KEYSTATE_UP)
		{
			keyStates[it->second] = KeyState::KEYSTATE_PRESSED;
		}
		else if (action == GLFW_RELEASE && curr == KeyState::KEYSTATE_DOWN)
		{
			keyStates[it->second] = KeyState::KEYSTATE_RELEASED;
		}
	}
}

void InputManagerGLFW::Update()
{
	for (int i = 0; i < changedKeys.size(); i++)
	{
		if (keyStates[changedKeys[i]] == KeyState::KEYSTATE_RELEASED)
		{
			keyStates[changedKeys[i]] = KeyState::KEYSTATE_UP;
		}
		else if (keyStates[changedKeys[i]] == KeyState::KEYSTATE_PRESSED)
		{
			keyStates[changedKeys[i]] = KeyState::KEYSTATE_DOWN;
		}
		changedKeys.erase(changedKeys.begin()+i);
	}
}

InputManagerGLFW::KeyState InputManagerGLFW::GetKeyState(KeyCode key)
{
	std::map<KeyCode, KeyState>::iterator it = keyStates.find(key);
	if (it != keyStates.end())
	{
		KeyState state = it->second;
		return  state;
	}
	else
	{
		return KEYSTATE_UNKNOWN;
	}
}
