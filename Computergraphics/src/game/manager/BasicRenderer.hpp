#ifndef BASICRENDERER_HPP
#define BASICRENDERER_HPP
#include "../../engine/manager/IRenderer.hpp"
#include "../../engine/components/renderable/CRenderable.hpp"
class BasicRenderer : IRenderer
{
public:
	BasicRenderer()
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
	}

	void RenderEntity(Entity* entity)
	{
		CRenderable* rend = entity->GetComponentOfType<CRenderable>();
		if (rend == nullptr || !rend->IsEnabled())
		{
			return;
		}
		glActiveTexture(GL_TEXTURE0);
		rend->mesh->Activate();
		rend->texture->Activate();
		rend->shader->Activate();
		GLuint camUniform = glGetUniformLocation(rend->shader->id, "cameraMatrix");
		glUniformMatrix4fv(camUniform, 1, false, (GLfloat*)&camera->GetCameraMatrix());
		GLuint scaleUniform = glGetUniformLocation(rend->shader->id, "transformScale");
		glUniform3f(scaleUniform, entity->transform.scale.x, entity->transform.scale.y, entity->transform.scale.z);
		GLuint posUniform = glGetUniformLocation(rend->shader->id, "transformPosition");
		glUniform3f(posUniform, entity->transform.position.x, entity->transform.position.y, entity->transform.position.z);
		if(rend->mesh->instances > 0)
		{ 
			glDrawArraysInstanced(GL_TRIANGLE_FAN, 0, 4, rend->mesh->instances);
		}
		else
		{
			glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		}
		rend->shader->Deactivate();
		rend->texture->Deactivate();
		rend->mesh->Deactivate();
	}
	
	void RenderEntitys(vector<Entity*> entitys)
	{
		for (int i = 0; i < entitys.size(); i++)
		{
			RenderEntity(entitys[i]);
		}
	}

	void SetActiveCamera(ICamera* camera)
	{
		this->camera = camera;
	}

	ICamera* GetActiveCamera()
	{
		return camera;
	}

private:
	ICamera* camera = nullptr;

};
#endif // !BASICRENDERER_HPP
