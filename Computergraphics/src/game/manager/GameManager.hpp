#ifndef GAMEMANAGER_HPP
#define GAMEMANAGAR_HPP
#include "../../engine/manager/IGameManager.hpp"
#include <iostream>
#include <GL/glew.h>
#include <GL/GL.h>
#include <GL/GLU.h>
#include <GLFW/glfw3.h>
#include "AssetManager.hpp"
#include "../../engine/entity/Entity.hpp"
#include "../../engine/components/renderable/CAnimationController.hpp"
#include "../../engine/animation/AnimationClip.hpp"

class GameManager : public IGameManager
{
public:
	GameManager(GLFWwindow* window, string appPath);
	void Update(double deltatime) override;
	void Render(double deltatime) override;
	static void Key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void Resize_callback(GLFWwindow * 	window, int x, int y);
	AnimationClip* GetUVAnimationClip(std::string name, std::vector<glm::vec2> idx, std::vector<float> duration);
	CAnimationController* GetPlayerAnimationController();
private:
	GLFWwindow* window;
	Entity* GetTerrain();

};
#endif // !GAMEMANAGER_HPP
