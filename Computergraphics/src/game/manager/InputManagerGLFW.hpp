#ifndef INPUTMANAGERGLFW_HPP
#define INPUTMANAGERGLFW_HPP
#include "GLFW/glfw3.h"
#include "../../engine/manager/IInputManager.hpp"
#include <map>
#include <vector>
class InputManagerGLFW : public IInputManager
{
public:
	InputManagerGLFW(GLFWwindow* window);
	virtual KeyState GetKeyState(KeyCode key);
	void KeyCallBack(GLFWwindow* window, int key, int scancode, int action, int mods);
	void Update();

private:
	GLFWwindow* window;
	std::vector<KeyCode> changedKeys;
	std::map<KeyCode, KeyState> keyStates;

	//Maps GLFW to belonging KeyCode
	std::map<int, KeyCode> glfwToKeyMap;
	//Maps KeyCode to belonging GLFW Key
	std::map<KeyCode, int> keyToGLFWMap = {
		//Mouse
	{Mouse_Button_Left, GLFW_MOUSE_BUTTON_LEFT},
	{Mouse_Button_Middle, GLFW_MOUSE_BUTTON_MIDDLE},
	{Mouse_Button_Right, GLFW_MOUSE_BUTTON_RIGHT},
	//Special Keys
	{Key_Backspace, GLFW_KEY_BACKSPACE},
	{Key_Tab, GLFW_KEY_TAB},
	{Key_Enter, GLFW_KEY_ENTER},
	{Key_Pause, GLFW_KEY_PAUSE},
	{Key_Caps_Log, GLFW_KEY_CAPS_LOCK},
	{Key_Escape, GLFW_KEY_ESCAPE},
	{Key_Space, GLFW_KEY_SPACE},
	{Key_Page_UP, GLFW_KEY_PAGE_UP},
	{Key_Page_Down, GLFW_KEY_PAGE_DOWN},
	{Key_End, GLFW_KEY_END},
	{Key_Home, GLFW_KEY_HOME},

	{Key_Arrow_Left, GLFW_KEY_LEFT},
	{Key_Arrow_Right, GLFW_KEY_RIGHT},
	{Key_Arrow_Up, GLFW_KEY_UP},
	{Key_Arrow_Down, GLFW_KEY_DOWN},

	{Key_Print_Screen, GLFW_KEY_PRINT_SCREEN},
	{Key_Insert, GLFW_KEY_INSERT},
	{Key_Delete, GLFW_KEY_DELETE},
	{Key_Numlock, GLFW_KEY_NUM_LOCK},

	{Key_Shift_Left, GLFW_KEY_LEFT_SHIFT},
	{Key_Shift_Right, GLFW_KEY_RIGHT_SHIFT},
	{Key_Control_Left, GLFW_KEY_LEFT_CONTROL},
	{Key_Control_Right, GLFW_KEY_RIGHT_CONTROL},
	{Key_Alt_Left, GLFW_KEY_LEFT_ALT},
	{Key_Alt_Right, GLFW_KEY_RIGHT_ALT},
	//Numbers
	{Key_0, GLFW_KEY_0},
	{Key_1, GLFW_KEY_1},
	{Key_2, GLFW_KEY_2},
	{Key_3, GLFW_KEY_3},
	{Key_4, GLFW_KEY_4},
	{Key_5, GLFW_KEY_5},
	{Key_6, GLFW_KEY_6},
	{Key_7, GLFW_KEY_7},
	{Key_8, GLFW_KEY_8},
	{Key_9, GLFW_KEY_9},
	//Alphabet
	{Key_A, GLFW_KEY_A},
	{Key_B, GLFW_KEY_B},
	{Key_C, GLFW_KEY_C},
	{Key_D, GLFW_KEY_D},
	{Key_E, GLFW_KEY_E},
	{Key_F, GLFW_KEY_F},
	{Key_G, GLFW_KEY_G},
	{Key_H, GLFW_KEY_H},
	{Key_I, GLFW_KEY_I},
	{Key_J, GLFW_KEY_J},
	{Key_K, GLFW_KEY_K},
	{Key_L, GLFW_KEY_L},
	{Key_M, GLFW_KEY_N},
	{Key_O, GLFW_KEY_P},
	{Key_Q, GLFW_KEY_Q},
	{Key_R, GLFW_KEY_R},
	{Key_S, GLFW_KEY_S},
	{Key_T, GLFW_KEY_T},
	{Key_U, GLFW_KEY_U},
	{Key_V, GLFW_KEY_V},
	{Key_W, GLFW_KEY_W},
	{Key_X, GLFW_KEY_X},
	{Key_Y, GLFW_KEY_Y},
	{Key_Z, GLFW_KEY_Z},

	//OEM Special Characters
	{Key_OEM_1, GLFW_KEY_SEMICOLON},
	//{Key_OEM_102, GLFW_KEY_WORLD_1},
	{Key_OEM_2, GLFW_KEY_SLASH},
	{Key_OEM_3, GLFW_KEY_GRAVE_ACCENT},
	{Key_OEM_4, GLFW_KEY_RIGHT_BRACKET},
	{Key_OEM_5, GLFW_KEY_BACKSLASH},
	{Key_OEM_6, GLFW_KEY_LEFT_BRACKET},
	{Key_OEM_7, GLFW_KEY_APOSTROPHE},
	//{Key_OEM_8, GLFW_KEY_WORLD_2},

	{Key_OEM_COMMA, GLFW_KEY_COMMA},
	{Key_OEM_MINUS, GLFW_KEY_MINUS},
	{Key_OEM_PERIOD, GLFW_KEY_PERIOD},
	{Key_OEM_PLUS, GLFW_KEY_EQUAL},


	//Keypad Numbers
	{Key_KP_0, GLFW_KEY_KP_0},
	{Key_KP_1, GLFW_KEY_KP_1},
	{Key_KP_2, GLFW_KEY_KP_2},
	{Key_KP_3, GLFW_KEY_KP_3},
	{Key_KP_4, GLFW_KEY_KP_4},
	{Key_KP_5, GLFW_KEY_KP_5},
	{Key_KP_6, GLFW_KEY_KP_6},
	{Key_KP_7, GLFW_KEY_KP_7},
	{Key_KP_8, GLFW_KEY_KP_8},
	{Key_KP_9, GLFW_KEY_KP_9},
	//Keypad Mathematics
	{Key_KP_Multiply, GLFW_KEY_KP_MULTIPLY},
	{Key_KP_Add, GLFW_KEY_KP_ADD},
	{Key_KP_Substract, GLFW_KEY_KP_SUBTRACT},
	{Key_KP_Decimal, GLFW_KEY_KP_DECIMAL},
	{Key_KP_Divide, GLFW_KEY_KP_DIVIDE},
	{Key_KP_Enter, GLFW_KEY_KP_ENTER},

	//Function Keys
	{Key_F1, GLFW_KEY_F1},
	{Key_F2, GLFW_KEY_F2},
	{Key_F3, GLFW_KEY_F3},
	{Key_F4, GLFW_KEY_F4},
	{Key_F5, GLFW_KEY_F5},
	{Key_F6, GLFW_KEY_F6},
	{Key_F7, GLFW_KEY_F7},
	{Key_F8, GLFW_KEY_F8},
	{Key_F9, GLFW_KEY_F9},
	{Key_F10, GLFW_KEY_F10},
	{Key_F11, GLFW_KEY_F11},
	{Key_F12, GLFW_KEY_F12},
	};
};
#endif // !INPUTMANAGER_HPP
