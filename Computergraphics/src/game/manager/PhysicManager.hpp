#ifndef PHYSICMANAGER_HPP
#define PHYSICMANAGER_HPP
#include "../../engine/manager/IPhysicManager.hpp"
#include "../../engine/serviceslocator/ServiceLocater.hpp"
#include "../../engine/manager/ISceneManager.hpp"
#include "../../engine/util/Debug.hpp"
#include "../../engine/components/physics/CRigidbody.hpp"
#include <vector>
class PhysicManager : public IPhysicManager
{
	void UpdatePhysics(double deltatime)
	{
		float delta = (float)deltatime;
		std::vector<Entity*> scene = ServiceLocator::GetService<ISceneManager>()->GetSceneList();
		for (int i = 0; i < scene.size(); i++)
		{
			Entity* entity = scene[i];
			CRigidBody* rbody = entity->GetComponentOfType<CRigidBody>();
			CBoxCollider2D* coll = entity->GetComponentOfType<CBoxCollider2D>();
			if (rbody == nullptr || !rbody->IsEnabled() || coll == nullptr)
			{
				continue;
			}
			Box2D* box = coll->GetBox();
			glm::vec3 acceleration = rbody->acceleration;
			glm::vec3 movement = acceleration * delta;
			entity->transform.position += movement;
			if (rbody != nullptr && rbody->IsEnabled())
			{
				for (int j = 0; j < scene.size(); j++)
				{
					Entity* other = scene[j];
					if (entity == other)
					{
						continue;
					}
					CBoxCollider2D* otherColl = other->GetComponentOfType<CBoxCollider2D>();
					if (otherColl == nullptr)
					{
						continue;
					}
					Box2D* otherBox = otherColl->GetBox();
					if (otherBox == nullptr)
					{
						continue;
					}
					if (box->DoesOverlap(otherBox))
					{
						box->UndoOverlap(otherBox);
					}
				}
			}
		}
	}

	bool CheckCollision(Entity* entity)
	{
		std::vector<Entity*> scene = ServiceLocator::GetService<ISceneManager>()->GetSceneList();
		CBoxCollider2D* coll = entity->GetComponentOfType<CBoxCollider2D>();
		Box2D* box = nullptr;
		if (coll != nullptr)
		{
			box = coll->GetBox();
		}
		if (box == nullptr)
		{
			return false;
		}
		for (int i = 0; i < scene.size(); i++)
		{
			if (scene[i] == entity)
			{
				continue;
			}
			CBoxCollider2D* otherColl = scene[i]->GetComponentOfType<CBoxCollider2D>();
			if (otherColl != nullptr && otherColl->IsEnabled())
			{
				Box2D* otherBox = otherColl->GetBox();
				if (otherBox != nullptr && box->DoesOverlap(otherBox))
				{
					return true;
				}
			}
		}
		return false;
	}

	virtual void CheckPosition(Entity* entity)
	{
		std::vector<Entity*> scene = ServiceLocator::GetService<ISceneManager>()->GetSceneList();
		CBoxCollider2D* coll = entity->GetComponentOfType<CBoxCollider2D>();
		Box2D* box = nullptr;
		if (coll != nullptr)
		{
			box = coll->GetBox();
		}
		if (box == nullptr)
		{
			return;
		}
		for (int i = 0; i < scene.size(); i++)
		{
			if (scene[i] == entity)
			{
				continue;
			}
			CBoxCollider2D* otherColl = scene[i]->GetComponentOfType<CBoxCollider2D>();
			if (otherColl != nullptr && otherColl->IsEnabled())
			{
				Box2D* otherBox = otherColl->GetBox();
				if (otherBox == nullptr || !box->DoesOverlap(otherBox))
				{
					continue;
				}
				box->UndoOverlap(otherBox);
				//Debug::Log(this, __func__, __LINE__, Debug::INFO, message);
			}
		}
	}

	vector<Collision> GetOverlaps(Entity* entity)
	{
		vector<Collision> result;
		std::vector<Entity*> scene = ServiceLocator::GetService<ISceneManager>()->GetSceneList();
		CBoxCollider2D* coll = entity->GetComponentOfType<CBoxCollider2D>();
		Box2D* box = nullptr;
		if (coll != nullptr)
		{
			box = coll->GetBox();
		}
		if (box == nullptr)
		{
			return result;
		}
		for (int i = 0; i < scene.size(); i++)
		{
			if (scene[i] == entity)
			{
				continue;
			}
			CBoxCollider2D* otherColl = scene[i]->GetComponentOfType<CBoxCollider2D>();
			if (otherColl != nullptr && otherColl->IsEnabled())
			{
				Box2D* otherBox = otherColl->GetBox();
				if (otherBox == nullptr || !box->DoesOverlap(otherBox))
				{
					continue;
				}
				glm::vec2 overlap = box->GetOverlapWithCoefficient(otherBox);
				Collision tmpColl = Collision(overlap, coll, otherColl);
				result.push_back(tmpColl);
			}
		}
		return result;
	}
};
#endif // !PHYSICMANAGER_HPP
