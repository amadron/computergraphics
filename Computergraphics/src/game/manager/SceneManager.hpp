#ifndef SCENEMANAGER_HPP
#define SCENEMANAGER_HPP
#include "../../engine/manager/ISceneManager.hpp"
#include <vector>
class SceneManager : ISceneManager
{
public:
	void AddEntity(Entity* entity)
	{
		sceneList.push_back(entity);
	}

	Entity* GetEntityByName(std::string name)
	{
		for (int i = 0; i < sceneList.size(); i++)
		{
			if (sceneList[i]->name == name)
			{
				return sceneList[i];
			}
		}
		return nullptr;
	}

	void RemoveEntity(Entity* entity)
	{
		for (int i = 0; i < sceneList.size(); i++)
		{
			if (sceneList[i] == entity)
			{
				sceneList.erase(sceneList.begin() + i);
			}
		}
	}

	const vector<Entity*> GetSceneList()
	{
		return sceneList;
	}

private:
	std::vector<Entity*> sceneList;
};
#endif // !SCENEMANAGER_HPP
