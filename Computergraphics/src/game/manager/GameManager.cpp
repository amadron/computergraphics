#include "GameManager.hpp"
#include "../../engine/serviceslocator/ServiceLocater.hpp"
#include "SceneManager.hpp"
#include "InputManagerGLFW.hpp"
#include "BasicRenderer.hpp"
#include "PhysicManager.hpp"

#include "../../engine/graphics/textures/ITexture.hpp"
#include "../../engine/graphics/shader/IShaderProgram.hpp"
#include "../../engine/util/Debug.hpp"
#include "../../engine/graphics/camera/ICamera.hpp"
#include "../../engine/graphics/camera/CameraOrthoGraphic.hpp"
#include "../../engine/tiled/TiledManager.hpp"
#include "../../engine/components/renderable/CRenderable.hpp"
#include "../../engine/components/renderable/CTextureAnimator.hpp"
#include "../../engine/components/renderable/CAnimationController.hpp"
#include "../../engine/animation/AnimationClip.hpp"
#include "../../engine/animation/AnimationTrack.hpp"
#include "../animation/UVGridFrame.hpp"
#include "../components/CPlayerInput.hpp"
#include "../components/CCharacterController.hpp";

InputManagerGLFW* inputManager;

GameManager::GameManager(GLFWwindow* window, string appPath)
{

	inputManager = new InputManagerGLFW(window);
	glfwSetKeyCallback(window, Key_callback);
	glfwSetWindowSizeCallback(window, Resize_callback);


	ServiceLocator::Provide<IInputManager>(inputManager);
	ServiceLocator::Provide<ISceneManager>((ISceneManager*)new SceneManager());
	ServiceLocator::Provide<IRenderer>((IRenderer*) new BasicRenderer());
	ServiceLocator::Provide<IPhysicManager>((IPhysicManager*) new PhysicManager());
	ServiceLocator::Provide<IAssetManager>((IAssetManager*) new AssetManager(appPath));

	ICamera* cam =(ICamera*) new CameraOrthoGraphic(glm::vec3(0, 0, 1000), glm::vec3(0, 0, 0), 10, 10, 0.1f, 2000);
	ServiceLocator::GetService<IRenderer>()->SetActiveCamera(cam);
	
	Entity* testChar = ServiceLocator::GetService<IAssetManager>()->Get2DEntity("Player", "assets/textures/characters/chara2.png");
	Box2D* testBox = new Box2D(&testChar->transform, 0.7f, 0.2f);
	CBoxCollider2D* testCollider = new CBoxCollider2D(testBox);
	testChar->AddComponent<CBoxCollider2D>(testCollider);
	CRigidBody* rbody = new CRigidBody();
	testChar->AddComponent<CRigidBody>(rbody);
	testChar->AddComponent<CPlayerInput>(new CPlayerInput());
	testChar->AddComponent<CCharacterController>(new CCharacterController());
	testChar->AddComponent<CTextureAnimator>(new CTextureAnimator(12, 8));
	testChar->GetComponentOfType<CTextureAnimator>()->SetUVsOfElement(1, 1);
	CAnimationController* pAnimController = GetPlayerAnimationController();
	testChar->AddComponent<CAnimationController>(pAnimController);
	pAnimController->SetActiveClip("up");
	pAnimController->StartActiveClip();
	//testChar->transform.position.z = 0.5f;

	Entity* otherTest = ServiceLocator::GetService<IAssetManager>()->Get2DEntity("Other", "assets/textures/test.jpg");
	otherTest->transform.position.x += 1.5f;
	Box2D* otherBox = new Box2D(&otherTest->transform, 1, 1);
	CBoxCollider2D* otherCollider = new CBoxCollider2D(otherBox);
	otherTest->AddComponent(otherCollider);

	Entity* otherTest2 = ServiceLocator::GetService<IAssetManager>()->Get2DEntity("Other2", "assets/textures/test.jpg");
	Box2D* otherBox2 = new Box2D(&otherTest2->transform, 1, 1);
	CBoxCollider2D* otherCollider2 = new CBoxCollider2D(otherBox2);
	otherTest2->transform.position.x += 1.5f;
	otherTest2->transform.position.y -= 1.5f;
	otherTest2->transform.position.z = -otherTest2->transform.position.y;
	otherTest2->AddComponent(otherCollider2);
	Entity* terrain = GetTerrain();
	if (terrain != nullptr)
	{
		ServiceLocator::GetService<ISceneManager>()->AddEntity(terrain);
	}
	ServiceLocator::GetService<ISceneManager>()->AddEntity(testChar);
	ServiceLocator::GetService<ISceneManager>()->AddEntity(otherTest);
	ServiceLocator::GetService<ISceneManager>()->AddEntity(otherTest2);
	this->window = window;
	
}

void GameManager::Resize_callback(GLFWwindow * window, int x, int y)
{
	
	glViewport(0, 0, x, y);
}

void GameManager::Key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{

	inputManager->KeyCallBack(window, key, scancode, action, mods);
}

void GameManager::Update(double deltatime)
{
	
	IInputManager* input = ServiceLocator::GetService<IInputManager>();
	IPhysicManager* physic = ServiceLocator::GetService<IPhysicManager>();
	ISceneManager* scene = ServiceLocator::GetService<ISceneManager>();
	if (scene != nullptr)
	{
		std::vector<Entity*> sceneList = scene->GetSceneList();
		for (int i = 0; i < sceneList.size(); i++)
		{
			sceneList[i]->Update(deltatime);
		}
	}
	//Update Rigidbodys
	if (physic != nullptr)
	{
		physic->UpdatePhysics(deltatime);
	}
	else
	{
		Debug::Log(this, __func__, __LINE__, Debug::INFO, "Physic Service is null!");
	}

	if (inputManager != nullptr)
	{
		inputManager->Update();
	}
}

void GameManager::Render(double deltatime)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//Rendering being
	vector<Entity*> scene = ServiceLocator::GetService<ISceneManager>()->GetSceneList();
	ServiceLocator::GetService<IRenderer>()->RenderEntitys(scene);
	Debug::CheckOpenGL(this, __func__, __LINE__);
	glfwSwapBuffers(window);
}

Entity* GameManager::GetTerrain()
{
	TiledManager* tiledManager = new TiledManager();
	IShaderProgram* terrainShader = ServiceLocator::GetService<IAssetManager>()->GetShader("assets/shader/defaultTerrain.vert", "assets/shader/defaultTerrain.frag");
	Tiled::TiledMap* map = tiledManager->GetMap("assets/maps/test.tmx");
	if (map != nullptr && map->layerList.size() > 0)
	{
		VAO* terrainVao = tiledManager->GetLayerVAO(map->layerList[0]);
		ITexture* terrainTexture = tiledManager->GetLayerTexture("assets/textures/testSplit/", 1, 744, "png");
		Entity* result = new Entity("Terrain");
		result->transform.position.z = -999;
		CRenderable* renderable = new CRenderable(terrainShader, terrainTexture, terrainVao);
		result->AddComponent(renderable);
		return result;
	}
	else
	{
		return nullptr;
	}
}

AnimationClip* GameManager::GetUVAnimationClip(std::string name, std::vector<glm::vec2> idx, std::vector<float> duration)
{
	if (idx.size() != duration.size())
	{
		Debug::Log(this, __func__, __LINE__, Debug::ERROR, "Amount of Indexes is not Equal Amount of Frame TimingS!");
		return nullptr;
	}
	AnimationClip* clip = new AnimationClip();
	clip->SetName(name);
	std::vector<AnimationFrame*> frames;
	for (int i = 0; i < idx.size(); i++)
	{
		AnimationFrame* tmp = new UVGridFrame(idx[i].x, idx[i].y, duration[i]);
		frames.push_back(tmp);
	}
	AnimationTrack* track = new AnimationTrack();
	track->SetFrames(frames);
	clip->AddAnimationTrack(track);
	
	return clip;
}

CAnimationController* GameManager::GetPlayerAnimationController()
{
	CAnimationController* result = new CAnimationController();
	std::vector<float> times = { 0.2f,0.2f,0.2f };
	std::vector<glm::vec2> up = { glm::vec2(3,0), glm::vec2(4,0), glm::vec2(5,0) };
	std::vector<glm::vec2> right = { glm::vec2(3,1), glm::vec2(4,1), glm::vec2(5,1) };
	std::vector<glm::vec2> left = { glm::vec2(3,2), glm::vec2(4,2), glm::vec2(5,2) };
	std::vector<glm::vec2> down = { glm::vec2(3,3), glm::vec2(4,3), glm::vec2(5,3)};
	AnimationClip* clipUp = GetUVAnimationClip("up",up, times);
	clipUp->SetLoop(true);
	clipUp->Continue();
	AnimationClip* clipDown = GetUVAnimationClip("down",down, times);
	clipDown->SetLoop(true);
	clipUp->Continue();
	AnimationClip* clipLeft = GetUVAnimationClip("left",left, times);
	clipLeft->SetLoop(true);
	clipLeft->Continue();
	AnimationClip* clipRight = GetUVAnimationClip("right",right, times);
	clipRight->SetLoop(true);
	clipRight->Continue();
	result->AddAnimationClip(clipUp);
	result->AddAnimationClip(clipDown);
	result->AddAnimationClip(clipRight);
	result->AddAnimationClip(clipLeft);
	return result;
}


