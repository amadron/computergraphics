#include "AssetManager.hpp"
#include "glm/glm.hpp"
#include "../../engine/graphics/shader/GLEWShaderProgram.hpp"
#include "../../engine/graphics/textures/GLEWTexture2D.hpp"
#include "../../engine/util/Debug.hpp"
#include "../../engine/components/IComponent.hpp"
#include "../../engine/components/renderable/CRenderable.hpp"
#include <fstream>

using namespace glm;

AssetManager::AssetManager(string exePath)
{
	FreeImage_Initialise();
	this->appPath = exePath.substr(0, exePath.find_last_of("\\"));
}

string AssetManager::GetAppPath()
{
	return string(this->appPath).append("\\");
}

string AssetManager::GetStringFromFile(string path)
{
	string line;
	string result;
	string fname = GetAppPath();
	fname.append(path);
	ifstream file(fname);
	if (file.is_open())
	{
		while (getline(file, line))
		{
			result.append(line);
			result.append("\n");
		}
	}
	else
	{
		printf("Error, could not open File: %s!!!!", fname.c_str());
	}
	return result;
}

ITexture* AssetManager::GetTexture2D(string path)
{
	Image* img = LoadImage(path);
	if (img == nullptr)
	{
		return nullptr;
	}
	GLuint textID = 0;
	glGenTextures(1, &textID);
	glBindTexture(GL_TEXTURE_2D, textID);
	GLint internalFormat = GL_RGB;
	if (img->format == GL_BGRA)
	{
		internalFormat = GL_RGBA;
	}
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, img->width, img->height, 0, img->format, GL_UNSIGNED_BYTE, img->data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	GLEWTexture2D* text = new GLEWTexture2D(textID,GL_TEXTURE_2D);
	text->Deactivate();
	Debug::CheckOpenGL(this, __func__, __LINE__);
	return (ITexture*) text;
}

ITexture* AssetManager::GetTexture3D(string files[], size_t arraySize)
{
	GLuint textID = 0;

	int layerCounter = 0;
	int width = 0;
	int height = 0;
	int dataSize = 0;
	bool allocated = false;
	for (int i = 0; i < arraySize; i++)
	{
		std::string tmpFile = files[i];
		Image* img = LoadImage(tmpFile);
		if (img != NULL && img->data != NULL)
		{
			if (width > 0 && img->width != width || height > 0 && img->height != height)
			{
				Debug::Log(this, __func__, __LINE__, Debug::ERROR, "Width or Height of the Images are Inconsistent");
				return NULL;
			}
			if (!allocated)
			{
				width = img->width;
				height = img->height;
				glGenTextures(1, &textID);
				glBindTexture(GL_TEXTURE_2D_ARRAY, textID);
				glTextureStorage3D(textID, 1, GL_RGBA8, width, height, arraySize);
				Debug::CheckOpenGL(this, __func__, __LINE__);

				allocated = true;
			}
			glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, width, height, 1, img->format, GL_UNSIGNED_BYTE, img->data);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP);
			Debug::CheckOpenGL(this, __func__, __LINE__);
		}
		else
		{
			Debug::Log(this, __func__, __LINE__, Debug::ERROR, string(files[i]).append("Could not be loaded"));
		}
	}

	GLEWTexture2D* resultTexture = new GLEWTexture2D(textID, GL_TEXTURE_2D_ARRAY);
	resultTexture->Deactivate();
	return (ITexture *)resultTexture;
}

Image* AssetManager::LoadImage(string path)
{
	string fname = GetAppPath();
	fname.append(path);
	FREE_IMAGE_FORMAT format;
	const char* constFname = fname.c_str();
	format = FreeImage_GetFIFFromFilename(constFname);
	if (format == FIF_UNKNOWN)
	{
		Debug::Log(this, __func__, __LINE__, Debug::ERROR, "Unknown Format!");
		return nullptr;
	}
	FIBITMAP* img = FreeImage_Load(format, fname.c_str());
	if (img == nullptr)
	{
		Debug::Log(this, __func__, __LINE__, Debug::ERROR, "Image could not be loaded!");
		return nullptr;
	}
	int bpp = FreeImage_GetBPP(img);
	Image* result = new Image();
	result->height = FreeImage_GetHeight(img);
	result->width = FreeImage_GetWidth(img);
	result->data = FreeImage_GetBits(img);
	FREE_IMAGE_TYPE internalType = FreeImage_GetImageType(img);
	if (internalType == FIT_BITMAP)
	{
		if (bpp == 24)
		{
			result->format = GL_BGR;
		}
		else if (bpp == 32)
		{
			result->format = GL_BGRA;
		}
	}
	else
	{
		if (bpp == 48 || bpp == 96)
		{

			result->format = GL_BGR;
		}
		else if (bpp == 64 || bpp == 128)
		{
			result->format = GL_BGRA;
		}
	}
	return result;
}



IShaderProgram* AssetManager::GetShader(string vertexShader, string fragmentShader)
{

	//Compile Vertex Shader
	GLuint vertID = glCreateShader(GL_VERTEX_SHADER);
	string vertCode = GetStringFromFile(vertexShader);
	const GLchar* vertsrc = vertCode.c_str();
	glShaderSource(vertID, 1, &vertsrc, 0);
	glCompileShader(vertID);

	//Vertex Shader properly compiled?
	GLint isCompiled = 0;
	glGetShaderiv(vertID, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(vertID, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(vertID, maxLength, &maxLength, &infoLog[0]);
		Debug::Log(this, __func__, __LINE__, Debug::ERROR, string(infoLog.begin(), infoLog.end()));
		glDeleteShader(vertID);

		return nullptr;
	}

	//Fragment Shader properly compiled?
	GLuint fragID = glCreateShader(GL_FRAGMENT_SHADER);
	string fragCode = GetStringFromFile(fragmentShader);
	const GLchar* fragsrc = fragCode.c_str();
	glShaderSource(fragID, 1, &fragsrc, 0);
	glCompileShader(fragID);

	glGetShaderiv(fragID, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(fragID, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(fragID, maxLength, &maxLength, &infoLog[0]);
		Debug::Log(this, __func__, __LINE__, Debug::ERROR, string(infoLog.begin(), infoLog.end()));

		glDeleteShader(vertID);
		glDeleteShader(fragID);
		return nullptr;
	}

	//Creating Programm and Link

	GLuint progID;
	progID = glCreateProgram();
	glAttachShader(progID, vertID);
	glAttachShader(progID, fragID);

	glLinkProgram(progID);
	//Programm Properly linked?
	glGetProgramiv(progID, GL_LINK_STATUS, &isCompiled);
	glDetachShader(progID, vertID);
	glDetachShader(progID, fragID);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(progID, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(progID, maxLength, &maxLength, &infoLog[0]);
		Debug::Log(this, __func__, __LINE__, Debug::ERROR, string(infoLog.begin(), infoLog.end()));

		glDeleteShader(vertID);
		glDeleteShader(fragID);
		glDeleteProgram(progID);
		return nullptr;
	}

	GLEWShaderProgram* prog = new GLEWShaderProgram(progID);
	return (IShaderProgram*) prog;
}

VAO* AssetManager::GetQuad(float width, float height)
{
	float halfX = width / 2;
	float halfY = height / 2;
	vec3 verts3[] = {
			vec3(-halfX, halfY, 0),
			vec3(halfX, halfY, 0),
			vec3(halfX, -halfY, 0),
			vec3(-halfX, -halfY, 0) };
	vec2 uvs2[] = {
		vec2(0,1),
		vec2(1,1),
		vec2(1,0),
		vec2(0,0)
	};

	GLuint vaoID;
	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);
	
	VBO* posVBO = new VBO();
	posVBO->attribID = AttributeLayout::Position;
	posVBO->Activate();
	glBufferData(GL_ARRAY_BUFFER, sizeof(verts3), verts3, GL_STATIC_DRAW);
	glEnableVertexAttribArray(posVBO->attribID);
	glVertexAttribPointer(posVBO->attribID, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	VBO* uvVBO = new VBO();
	uvVBO->attribID = AttributeLayout::UV;
	uvVBO->Activate();
	glBufferData(GL_ARRAY_BUFFER, sizeof(uvs2), uvs2, GL_STATIC_DRAW);
	glEnableVertexAttribArray(uvVBO->attribID);
	glVertexAttribPointer(uvVBO->attribID, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glBindVertexArray(0);
	VAO* vao = new VAO(vaoID);
	vao->vboList.push_back(posVBO);
	vao->vboList.push_back(uvVBO);
	return vao;
}

void AssetManager::CleanUp()
{
	FreeImage_DeInitialise();
}

Entity* AssetManager::Get2DEntity(string name, string texturePath)
{
	Entity* result = new Entity(name);
	ITexture* text = GetTexture2D(texturePath);
	VAO* vao = GetQuad(1, 1);
	IShaderProgram* shader = GetShader("assets/shader/unlitTextured.vert","assets/shader/unlitTextured.frag");
	CRenderable* renderable = new CRenderable(shader, text, vao);
	result->AddComponent(renderable);
	return result;
}
