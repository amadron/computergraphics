#ifndef IITEM_HPP
#define IITEM_HPP
#include "../../engine/entity/Entity.hpp"
class IItem {
public:
	virtual void Use(Entity* entity) = 0;
	virtual void Update(Entity* entity) = 0;
	virtual void EndOfUse(Entity* entity) = 0;
};
#endif // !IITEM_HPP

