// Computergraphics.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include "pch.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "src/game/manager/GameManager.hpp"

IGameManager* gameManager;
GLFWwindow* window;
double lastTime = 0;

bool Init();
bool InitGLFW();
bool InitGLEW();
void CleanUp();



int main(int argc, char** argv)
{

	if (!Init())
	{
		std::cout << "Error on Initializing!" << std::endl;
		CleanUp();
		return -1;
	}
	gameManager = new GameManager(window, argv[0]);
	
	while (!glfwWindowShouldClose(window) && !gameManager->quit)//(!gameManager->quit)
	{
		
		double tmpTime = glfwGetTime();
		double deltatime = tmpTime - lastTime;
		lastTime = tmpTime;
		glfwPollEvents();
		gameManager->Update(deltatime);
		gameManager->Render(deltatime);
		glfwPollEvents();
	}
	
	CleanUp();
}

void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}


bool Init()
{
	if (!InitGLFW())
	{
		return false;
	}
	if (!InitGLEW())
	{
		return false;
	}	
	return true;
}

bool InitGLFW()
{
	if (!glfwInit())
	{
		return false;
	}
	glfwSetErrorCallback(error_callback);
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	window = glfwCreateWindow(1024, 800, "Prototype", NULL, NULL);
	if (!window)
	{
		std::cout << "Error on Initializing Window with GLFW3! \n";
		return false;
	}
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	return true;
}

bool InitGLEW()
{
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Error on Initializing Glew! \n";
		return false;
	}
	glEnable(GL_TEXTURE_2D);
	return true;
}

void CleanUp()
{
	glfwDestroyWindow(window);
	glfwTerminate();
}
